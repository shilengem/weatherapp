package net.iqbusiness.exceptions;

public class InvalidCityException extends Exception{
    public InvalidCityException(String s){
        super(s);
    }

    public InvalidCityException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidCityException(Throwable cause) {
        super(cause);
    }
}
