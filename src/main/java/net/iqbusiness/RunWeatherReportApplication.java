package net.iqbusiness;

import net.iqbusiness.interfaces.WeatherReportViewer;
import net.iqbusiness.models.Weather;
import net.iqbusiness.services.*;
import org.apache.log4j.Logger;

import java.util.Properties;
import java.util.Scanner;

public class RunWeatherReportApplication {

    private static final Logger LOGGER = Logger.getLogger(RunWeatherReportApplication.class);

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        Properties prop = new Properties();
        OpenWeatherMapAPIService openWeatherMapReport = new OpenWeatherMapAPIService();
        WeatherAPIService weatherAPIReport = new WeatherAPIService();

        String option = "";

        LOGGER.info("Welcome To The Weather Report Application");
        LOGGER.info("Enter one of the following cities to view the weather report:" +
                "\n 1. Johannesburg " +
                "\n 2. Pretoria" +
                "\n 3. Durban" +
                "\n 4. Rustenburg" +
                "\n 5. Cape Town");

        do {
            try {
                LOGGER.info("Please enter city to get weather report: ");
                String city = scan.nextLine();

                if(city.equalsIgnoreCase("johannesburg") || city.equalsIgnoreCase("pretoria") || city.equalsIgnoreCase("durban") || city.equalsIgnoreCase("rustenburg") ||city.equalsIgnoreCase("cape town")){

                    LOGGER.info("Please enter 1 to get report using OpenWeatherAPI OR enter 2 using WeatherAPI ");
                    int apiChoice = scan.nextInt();

                    if(apiChoice==1){
                        prop.load(RunWeatherReportApplication.class.getClassLoader().getResourceAsStream("config.properties"));
                        String apiKey = prop.getProperty("API_KEY");

                        Weather weather = openWeatherMapReport.getWeatherReport(city,apiKey);
                        //List<Forecast>l = o.getForecastReport(city,apiKey);
                        LOGGER.info("Press 1 to view weather on console or 2 to view weather on GUI: ");
                        int displayOption = scan.nextInt();

                        if(displayOption == 1){
                            WeatherReportViewer printWeather = new WeatherReporConsoletViewer();
                            printWeather.displayWeatherReport(weather);
                            //WeatherReportViewer p = new WeatherReporConsoletViewer();
                            //p.displayForecastReport(l);
                            LOGGER.info("OPENWEATHER API REPORT ");
                        }
                        else if(displayOption == 2){
                            WeatherReportViewer printWeather = new WeatherReporGuiViewer();
                            printWeather.displayWeatherReport(weather);
                        }
                    }
                    else if(apiChoice == 2){
                        prop.load(RunWeatherReportApplication.class.getClassLoader().getResourceAsStream("config.properties"));
                        String apiKey = prop.getProperty("API_KEY1");

                        Weather weather = weatherAPIReport.getWeatherReport(city,apiKey);
                        LOGGER.info("Press 1 to view weather on console or 2 to view weather on GUI: ");
                        int displayOption = scan.nextInt();

                        if(displayOption == 1){
                            WeatherReportViewer printWeather = new WeatherReporConsoletViewer();
                            printWeather.displayWeatherReport(weather);
                            LOGGER.info("WEATHERAPI API REPORT ");
                        }
                        else if(displayOption == 2){
                            WeatherReportViewer printWeather = new WeatherReporGuiViewer();
                            printWeather.displayWeatherReport(weather);
                        }
                    }
                    option = scan.nextLine();
                }
                else{
                    LOGGER.info("Invalid city ");
                }

                LOGGER.info("Please Enter 'y' if you want to check weather for another City or any other key to exit.");
                option = scan.nextLine();

            }catch (Exception io) {
                LOGGER.error(io.getMessage());
            }

        } while(option.equalsIgnoreCase("y"));
    }
}
