package net.iqbusiness.services;

import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;
import kong.unirest.json.JSONObject;
import net.iqbusiness.interfaces.WeatherReport;
import net.iqbusiness.models.Weather;
import org.apache.log4j.Logger;

public class WeatherAPIService implements WeatherReport {

    private static final Logger LOGGER = Logger.getLogger(WeatherAPIService.class);

    @Override
    public Weather getWeatherReport(String city, String apiKey) {
        try {
            String urlString = String.format("http://api.weatherapi.com/v1/current.json?key=%s&q=%s",apiKey,city);
            JSONObject response =
                    Unirest.get(urlString)
                            .asJson()
                            .getBody()
                            .getObject();

            HttpResponse<JsonNode> httpResponse =
                    Unirest.get(urlString).asJson();
            httpResponse.getStatus();



            String temp = response.getJSONObject("current").getString("temp_c");
            String humidity = response.getJSONObject("current").getString("wind_kph");
            String windSpeed = response.getJSONObject("current").getString("humidity");
            String cloudiness = response.getJSONObject("current").getString("cloud");

            Weather weather = new Weather(temp,humidity,windSpeed,cloudiness);
            return weather;
        }
        catch (Exception e) {
            LOGGER.error(e.getMessage());
            return null;
        }
    }

}

