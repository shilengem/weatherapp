package net.iqbusiness.validations;

import net.iqbusiness.exceptions.InvalidCityException;

import java.util.regex.Pattern;

public class ValidateCity {

    public static void validateCity(String city) throws InvalidCityException {
        if(city.equals("")){
            throw new InvalidCityException("Empty city provided");
        }
        else if(Pattern.compile("[0-9]").matcher(city).find()){
            throw new InvalidCityException("City contains numbers");
        }
    }
}
