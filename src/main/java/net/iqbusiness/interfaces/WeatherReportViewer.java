package net.iqbusiness.interfaces;

import net.iqbusiness.models.Forecast;
import net.iqbusiness.models.Weather;

import java.util.List;

public interface WeatherReportViewer {
    void displayWeatherReport(Weather weather);
    void displayForecastReport(List<Forecast>l);
}
