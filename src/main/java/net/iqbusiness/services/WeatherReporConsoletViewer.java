package net.iqbusiness.services;

import net.iqbusiness.interfaces.WeatherReportViewer;
import net.iqbusiness.models.Forecast;
import net.iqbusiness.models.Weather;
import org.apache.log4j.Logger;

import java.util.List;

public class WeatherReporConsoletViewer implements WeatherReportViewer {

    private static final Logger LOGGER = Logger.getLogger(WeatherReporConsoletViewer.class);

    @Override
    public void displayWeatherReport(Weather weather) {
        try {
            String result = weather.toString();
            LOGGER.info(result);
        }
        catch (Exception e){
            LOGGER.error(e.getMessage());
        }
    }

    @Override
    public void displayForecastReport(List<Forecast>l) {

        String f = l.toString();
        LOGGER.info(f.substring(1,f.length()-1));

    }
}
