package net.iqbusiness.interfaces;

import net.iqbusiness.models.Forecast;

import java.util.List;

public interface ForecastReport {

    List<Forecast> getForecastReport(String city, String apiKey) throws Exception;
}
