package net.iqbusiness.validations;

import net.iqbusiness.exceptions.InvalidKeyException;

public class ValidateAPIKey {

    public static void validateApiKey(String apiKey) throws InvalidKeyException {
        if(!apiKey.equalsIgnoreCase("7be2384a89072521d9eef27cf4683047")){

            throw new InvalidKeyException("Invalid Key, please check if your api key is correct");
        }
    }
}
