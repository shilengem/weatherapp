package net.iqbusiness;

import net.iqbusiness.models.Weather;
import net.iqbusiness.services.WeatherAPIService;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class WeatherApiWeatherReportTest {


    @Test
    public void weatherPositiveResponseTest() {
        String city = "Pretoria";
        String apiKey = "a096f6e3b0844d9794592159200210";
        WeatherAPIService weatherReportImp = new WeatherAPIService();
        Weather weather = weatherReportImp.getWeatherReport(city,apiKey);

        assertNotNull("Weather is null", weather);
        assertNotNull("Temperature is null", weather.getTemp());
        assertNotNull("Cloudiness is null", weather.getCloudiness());
        assertNotNull("Humidity is null", weather.getHumidity());
        assertNotNull("Wind Speed is null", weather.getWindSpeed());
    }

    @Test
    public void emptyCityStringTest() {
        String city = "";
        String apiKey = "a096f6e3b0844d9794592159200210";
        WeatherAPIService weatherReportImp = new WeatherAPIService();
        Weather weather = weatherReportImp.getWeatherReport(city,apiKey);

        assertNull("Invalid city",weather);
    }

    @Test
    public void specialCharactersCityStringTest() {
        String city = "@#$%^&&*";
        String apiKey = "a096f6e3b0844d9794592159200210";
        WeatherAPIService weatherReportImp = new WeatherAPIService();
        Weather weather = weatherReportImp.getWeatherReport(city,apiKey);
        assertNull("Invalid city",weather);

    }

    @Test
    public void garbageCityStringTest() {
        String city = "123213asdsdSDSAD";
        String apiKey = "a096f6e3b0844d9794592159200210";
        WeatherAPIService weatherReportImp = new WeatherAPIService();
        Weather weather = weatherReportImp.getWeatherReport(city,apiKey);

        assertNull("Invalid city",weather);
    }

    @Test
    public void numbersCityStringTest() {
        String city = "123213";
        String apiKey = "a096f6e3b0844d9794592159200210";
        WeatherAPIService weatherReportImp = new WeatherAPIService();
        Weather weather = weatherReportImp.getWeatherReport(city,apiKey);

        assertNull("Invalid city",weather);
    }

    @Test
    public void invalidAPIKeyTest() {
        String city = "Pretoria";
        String apiKey = "a096f6e3b0844d9794592159200210aa";
        WeatherAPIService weatherReportImp = new WeatherAPIService();
        Weather weather = weatherReportImp.getWeatherReport(city,apiKey);

        assertNull("Invalid API Key",weather);
    }
}
