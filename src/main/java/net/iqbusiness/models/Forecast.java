package net.iqbusiness.models;

import java.util.Objects;

public class Forecast {

    private String day;
    private String temp;
    private String humidity;
    private String windSpeed;
    private String cloudiness;

    public Forecast(String day, String temp, String humidity, String windSpeed, String cloudiness) {
        this.day = day;
        this.temp = temp;
        this.humidity = humidity;
        this.windSpeed = windSpeed;
        this.cloudiness = cloudiness;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }

    public String getHumidity() {
        return humidity;
    }

    public void setHumidity(String humidity) {
        this.humidity = humidity;
    }

    public String getWindSpeed() {
        return windSpeed;
    }

    public void setWindSpeed(String windSpeed) {
        this.windSpeed = windSpeed;
    }

    public String getCloudiness() {
        return cloudiness;
    }

    public void setCloudiness(String cloudiness) {
        this.cloudiness = cloudiness;
    }

    @Override
    public String toString() {
        return  "Day: " + day + "\n" +
                "Current Temparature: " + temp + "\n" +
                "Current Humidity: " + humidity + "\n" +
                "Wind Speeds: " + windSpeed + "\n" +
                "Cloudiness: " + cloudiness + "\n";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Forecast forecast = (Forecast) o;
        return Objects.equals(day, forecast.day) &&
                Objects.equals(temp, forecast.temp) &&
                Objects.equals(humidity, forecast.humidity) &&
                Objects.equals(windSpeed, forecast.windSpeed) &&
                Objects.equals(cloudiness, forecast.cloudiness);
    }

    @Override
    public int hashCode() {
        return Objects.hash(day, temp, humidity, windSpeed, cloudiness);
    }
}
