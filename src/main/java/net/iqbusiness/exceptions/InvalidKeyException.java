package net.iqbusiness.exceptions;

public class InvalidKeyException extends Exception {

   public InvalidKeyException(String s){
        super(s);
    }
}
